import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JokespageComponent } from './jokespage.component';

describe('JokespageComponent', () => {
  let component: JokespageComponent;
  let fixture: ComponentFixture<JokespageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JokespageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JokespageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
