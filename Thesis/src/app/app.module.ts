import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { NotfoundpageComponent } from './notfoundpage/notfoundpage.component';
import { RouterModule, Routes } from '@angular/router';
import {MatToolbarModule} from '@angular/material/toolbar';
import { AboutpageComponent } from './aboutpage/aboutpage.component';
import { JokespageComponent } from './jokespage/jokespage.component';
import { LoginpageComponent } from './loginpage/loginpage.component';

const appRoutes: Routes = [
  {path: '', component: HomepageComponent},
  {path: '**', component: NotfoundpageComponent},
  {path: 'about', component: AboutpageComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    NotfoundpageComponent,
    AboutpageComponent,
    JokespageComponent,
    LoginpageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    MatToolbarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
