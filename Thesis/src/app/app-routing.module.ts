import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from 'src/app/homepage/homepage.component';
import { NotfoundpageComponent } from 'src/app/notfoundpage/notfoundpage.component';
import { AboutpageComponent } from 'src/app/aboutpage/aboutpage.component';
import { JokespageComponent } from './jokespage/jokespage.component';
import { LoginpageComponent } from './loginpage/loginpage.component';

const routes: Routes = [
  { path: '', component: HomepageComponent},
  { path: 'aboutpage/aboutpage.component.html', component: AboutpageComponent},
  { path: 'jokespage/jokespage.component.html', component: JokespageComponent},
  { path: 'loginpage/loginpage.component.html', component: LoginpageComponent},
  { path: '**', component: NotfoundpageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
